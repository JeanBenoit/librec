package librec.ranking;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;

import happy.coding.io.FileIO;
import happy.coding.io.Logs;
import happy.coding.io.Strings;
import librec.data.DenseMatrix;
import librec.data.DenseVector;
import librec.data.DiagMatrix;
import librec.data.Grid;
import librec.data.ItemContext;
import librec.data.Location;
import librec.data.Poi2Times;
import librec.data.SparseMatrix;
import librec.data.SparseVector;
import librec.data.VectorEntry;
import librec.data.WorldGrid;
import librec.intf.ContextRecommender;

/**
 * <h3>GeoMF-TD : GeoMF with Time Dependencies
 *  
 * @author jb.griesner
 * 
 */

public class GeoMFTD extends ContextRecommender {

	// optimization parameters
	private double sigma;
	private double lambda;
	
	// factorized users' activity areas matrix
	protected DenseMatrix X, last_X;

	// factorized POIs influence areas matrix
	protected DenseMatrix Y, last_Y;

	// matrix of grid
	private WorldGrid worldGrid;
	
	/* mind, lat/lon 2D-points 
	 * __ __ __ __ __ 
	 *|NW|__|__|__|NE| 
	 *|__|__|__|__|__|
	 *|__|__|__|__|__| 
	 *|SW|__|__|__|SE|
	 *
	 */	
	private Point2D.Double nePoint;
	private Point2D.Double swPoint;

	// {item, list{POI2Times from item POI}}
	public static Map<Integer, ArrayList<Poi2Times>> fromPOI;
	// {item, list{POI2Times to item POI}}
	public static Map<Integer, ArrayList<Poi2Times>> toPOI;
		
	/**
	 * @return Constructor
	 */
	public GeoMFTD(SparseMatrix trainMatrix, SparseMatrix testMatrix, int fold) {
		super(trainMatrix, testMatrix, fold);
		isRankingPred = true; // item recommendation
		algoName = "GeoMFTD";		
		try {
			// fill itemContexts : itemId - ic
			readContext();
			// create grids
			createWorldGrid();
			sigma = 1000000;
		} catch (Exception e) {
			Logs.error(e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	protected void buildModel() throws Exception {
		// 3 steps for each iteration
		for (int iter = 1; iter <= numIters; iter++) {

			// Step 1: update P;
			DenseMatrix Qt = Q.transpose();
			DenseMatrix QtQ = Qt.mult(Q);
			
			for (int u = 0; u < numUsers; u++) {
				if (verbose && (u + 1) % 500 == 0)
					Logs.debug("{}{} buildModel at iteration = {}, User = {}/{}", algoName, foldInfo, iter, u + 1, numUsers);
				
				// Y * x_u
				DenseVector Yxu = Y.mult(X.row(u));
				
				// diagonal matrix W^u for each user
				DiagMatrix Wu = DiagMatrix.eye(numItems); 				
				SparseVector ru = trainMatrix.row(u);
				for (VectorEntry ve : ru) {
					int i = ve.index();
					Wu.add(i, i, 1.0 + Math.log10(1.0 + (ve.get() * 100.0))); 
				}

				// binarize real values
				for (VectorEntry ve : ru)
					ve.set(ve.get() > 0 ? 1 : 0);

				// Cu = Wu - I
				DiagMatrix WuI = Wu.minus(1);
				
				// Qt * (Wu - I) * Q + QtQ
				DenseMatrix QtCuQ = QtQ.add(Qt.mult(WuI).mult(Q));
				// (QtCuQ + gamma * I)^-1
				DenseMatrix QtCuQgamma = (QtCuQ.add(DiagMatrix.eye(numFactors).scale(regU))).inv();
								
				// rYxu = r_u - Y * x_u
				DenseVector rYxu = new DenseVector(numItems);
				rYxu.init(0.0);
				for (VectorEntry ve : ru) {
					int i = ve.index();
					rYxu.add(i, (ve.get() - Yxu.get(i)));
				}				
				
				// Qt * (Wu - I) * (ru - Yxu)
				DenseVector QtCuRuYxu = Qt.mult(WuI).mult(rYxu);
				
				// Qt(r_u - Yx_u)
				DenseVector QtRuYxu = Qt.mult(rYxu);				

				// QtCuQgamma * (QtCuRuYxu + QtRuYxu)
				DenseVector pu = QtCuQgamma.mult(QtCuRuYxu.add(QtRuYxu));

				// udpate user factors
				P.setRow(u, pu);
			}

			// Step 2: update Q;
			DenseMatrix Pt = P.transpose();
			DenseMatrix PtP = Pt.mult(P);
			
			for (int i = 0; i < numItems; i++) {
				if (verbose && (i + 1) % 500 == 0)
					Logs.debug("{}{} buildModel at iteration = {}, Item = {}/{}", algoName, foldInfo, iter, i + 1, numItems);

				// X * y_i
				DenseVector Xyi = X.mult(Y.row(i));
				
				// diagonal matrix W^i for each item
				DiagMatrix Wi = DiagMatrix.eye(numUsers);
				SparseVector ri = trainMatrix.column(i);
				for (VectorEntry ve : ri) {
					int u = ve.index();
					Wi.add(u, u, 1.0 + Math.log10(1.0 + (ve.get() * 100.0))); 
				}

				// binarize real values
				for (VectorEntry ve : ri)
					ve.set(ve.get() > 0 ? 1 : 0);

				// WiI = Wi - I
				DiagMatrix WiI = Wi.minus(1); 

				// Pt * (Wi - I) * P + PtP
				DenseMatrix PtWiIPPtP = PtP.add(Pt.mult(WiI).mult(P));
				// (PtCiP + gamma * I)^-1
				DenseMatrix PtCiPgamma = (PtWiIPPtP.add(DiagMatrix.eye(numFactors).scale(regI))).inv();
				
				// rYxu = r_u - Y * x_u
				DenseVector rXyi = new DenseVector(numUsers);
				rXyi.init(0.0);
				for (VectorEntry ve : ri) {
					int j = ve.index();
					rXyi.add(j, (ve.get() - Xyi.get(j)));
				}	
				
				// Pt * (Wi - I) * (ri - X * y_i)
				DenseVector PtCirXyi = Pt.mult(WiI).mult(rXyi);

				// Pt(r_i - Xy_i)
				DenseVector PtRiXyi = Pt.mult(rXyi);
				
				DenseVector yi = PtCiPgamma.mult(PtCirXyi.add(PtRiXyi));

				// udpate item factors
				Q.setRow(i, yi);
			}
			
			// Step 3: update users' area factors X;
			DenseMatrix Yt = Y.transpose();
			
			for (int user = 0; user < numUsers; user++) {
				if (verbose && (user + 1) % 500 == 0)
					Logs.debug("{}{} buildModel at iteration = {}, X area = {}/{}", algoName, foldInfo, iter, user + 1, numUsers);
				
				// Y * x_u
				DenseVector xu = X.row(user);
				DenseVector Yxu = Y.mult(xu);
				
				// Q * p_u
				DenseVector Qpu = Q.mult(P.row(user));
				
				// diagonal matrix W^u for each user
				DiagMatrix Wu = DiagMatrix.eye(numItems); 				
				SparseVector ru = trainMatrix.row(user);
				for (VectorEntry ve : ru) {
					int i = ve.index();
					Wu.add(i, i, 1.0 + Math.log10(1.0 + (ve.get() * 100.0))); 
				}
				
				// Yt * W^u
				DenseMatrix YtW = Yt.mult(Wu);
				
				// r_u - Q * p_u
				DenseVector rQpu = new DenseVector(numItems);
				rQpu.init(0.0);
				for (VectorEntry ve : ru) {
					int k = ve.index();
					rQpu.add(k, (ve.get() - Qpu.get(k)));
				}	
				
				// Yt * W^u * (Y * x_u - (r_u - Q * p_u))
				DenseVector YxurQp = Yxu.minus(rQpu);
				DenseVector gradient_u = YtW.mult(YxurQp);
				gradient_u.add(lambda);
				for (int gradId = 0; gradId < gradient_u.getData().length; gradId++) {
					double gv = gradient_u.get(gradId);
					gradient_u.set(gradId, gv * 0.0001);
				}
				
				// x_u
				DenseVector x_u = proj(xu.minus(gradient_u));
				
				// update X 
				X.setRow(user, x_u);				
			}
			
		} // end each iteration

	}

	private DenseVector proj(DenseVector grad) {
		DenseVector result = new DenseVector(grad);
		for(int index = 0; index < result.getData().length; index++){
			if(result.get(index) <= 0.0){
				result.set(index, 0.0);
			}
		}
		return result;
	}

	@Override
	protected void initModel() throws Exception {
		super.initModel();

		lambda = paramOptions.getDouble("-lambda", 0.0);
		
		X = new DenseMatrix(numUsers, worldGrid.getGridsNum());
		X.init(0.0);

		Y = new DenseMatrix(numItems, worldGrid.getGridsNum());
		Y.init(0.0);
		// compute pois's influence areas vectors
		for (int innerItemId = 0; innerItemId < numItems; innerItemId++) {			
			DenseVector poiInfluenceArea = getPoiInfluenceArea(innerItemId);					
			Y.setRow(innerItemId, poiInfluenceArea);
		}	
	}

	private DenseVector getPoiInfluenceArea(int i) throws NoSuchAuthorityCodeException, FactoryException {
		DenseVector result = new DenseVector(worldGrid.getGridsNum());
		
		result.init(0.0);
		
		double itemLatitude = itemContexts.get(i).getLocation().getLatitude();
		double itemLongitude = itemContexts.get(i).getLocation().getLongitude();
		
		int itemXGrid = itemContexts.get(i).getXGrid();
		int itemYGrid = itemContexts.get(i).getYGrid();
		
//		int minXGrid = ( itemXGrid > 5 ? itemXGrid - 5 : 0);
//		int maxXGrid = ( itemXGrid < worldGrid.getHeight() - 5 ? itemXGrid + 5 : worldGrid.getHeight() );
//		int minYGrid = (itemYGrid > 5 ? itemYGrid - 5 : 0);
//		int maxYGrid = ( itemYGrid < worldGrid.getWidth() - 5 ? itemYGrid + 5 : worldGrid.getWidth() );
		
		int minXGrid = 0;
		int maxXGrid = worldGrid.getHeight();
		int minYGrid = 0;
		int maxYGrid = worldGrid.getWidth();
				
		for (int xGrid = minXGrid; xGrid < maxXGrid; xGrid++) {
			for (int yGrid = minYGrid; yGrid < maxYGrid; yGrid++) {
								
				GeodeticCalculator tempGc = new GeodeticCalculator(CRS.decode("EPSG:3857"));	
				Grid aGrid = worldGrid.getGrid()[xGrid][yGrid];
				tempGc.setStartingGeographicPoint(itemLongitude, itemLatitude);
				tempGc.setDestinationGeographicPoint(aGrid.getLongitude(), aGrid.getLatitude());
				double distance = tempGc.getOrthodromicDistance();
				
				double influence = gaussian(distance / sigma) / sigma;
				
				long diffTime = computeAverageTime(aGrid, i);
				double influenceCoeff = getInfluenceCoeff(diffTime, influence);
				
				
				result.set(aGrid.getGridId(), influence * influenceCoeff);
			}
		}		
		if (verbose && (i + 1) % 500 == 0)
			Logs.debug("{}{} initModel -> item = {}/{}, [x: {}, y: {}] [lat: {}, lon: {}] influence area bounds: [x:{}->{}, y:{}>{}]", algoName, foldInfo, i + 1, numItems, itemXGrid, itemYGrid, itemLatitude, itemLongitude, minXGrid, maxXGrid, minYGrid, maxYGrid);
		return result;
	}

	private double getInfluenceCoeff(long diffTime, double influence) {
		double result = 1.0;
		if(influence > 0.1){
			if(diffTime > 3600000){
				result = 0.5;
			}
			//result = 1.0 + gaussian(diffTime / (1000.0 * 60 * 60)); //Math.log10(1 + (days(diffTime) * 10000.0)); 
		}		
		return result;
	}

	private long computeAverageTime(Grid aGrid, int i) {
		long result = 0;
		int howmany = 0;
		ArrayList<Integer> poisInsideGrid = aGrid.getIdsPOI();
		ArrayList<Poi2Times> poisTuples = fromPOI.get(i);
		
		if(poisInsideGrid != null && !poisInsideGrid.isEmpty() && poisTuples != null && !poisTuples.isEmpty()){
			for(Poi2Times poi2time : poisTuples){
				int toItemId = rateDao.getItemId(poi2time.rawPOI2);
				for(Integer poi : poisInsideGrid){				
					if(toItemId == poi){						
						long tim = Long.parseLong(poi2time.time12);
						int howmanyt = Integer.parseInt(poi2time.times);
						result = ((result * howmany) + (tim * howmanyt)) / (howmany + howmanyt);
						howmany += howmanyt;
					}
				}
			}
		}
				
		return result;
	}

	/**
	 * @return prediction method
	 */
	@Override
	protected double predict(int u, int j) throws Exception {
		double result = DenseMatrix.rowMult(P, u, Q, j) + DenseMatrix.rowMult(X, u, Y, j);
		// Logs.debug("user:" + u + " for item : " + j + " ---> " + result);
		return result;
	}

	/**
	 * @return Create grid data structure
	 */
	private void createWorldGrid() {
		int granularity = paramOptions.getInt("-granularity");

		Logs.debug("(mind! lat/lng) nePoint: [{} , {}], swPoint: [{} , {}]", nePoint.getX(), nePoint.getY(), swPoint.getX(), swPoint.getY());

		try {
			worldGrid = new WorldGrid(granularity, swPoint, nePoint);
		} catch (FactoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return Read POI latitude-longitude and average Times between POI
	 */
	protected void readContext() throws Exception {
		String contextPath = cf.getPath("dataset.social");
		String timePath = cf.getPath("dataset.time");
		
		Logs.debug("Context dataset: {}", Strings.last(contextPath, 38));
		Logs.debug("Time dataset: {}", Strings.last(timePath, 38));
		
		readLoc(contextPath);		
		readTimes(timePath);		
	}

	private void readTimes(String timePath) throws IOException {
		fromPOI = new HashMap<Integer, ArrayList<Poi2Times>>();
		toPOI = new HashMap<Integer, ArrayList<Poi2Times>>();
		
		BufferedReader brTime = FileIO.getReader(timePath);
		
		String line = null;
		Poi2Times p2t = null;
		
		while ((line = brTime.readLine()) != null) {
			String[] data = line.split("[ \t,]");

			String poi1 = data[0];
			String poi2 = data[1];
			String times = data[2];
			String difftime = data[3];
			
			p2t = new Poi2Times(poi1, poi2, times, difftime);
			
			int innerpoi1;
			int innerpoi2;
			try{
				innerpoi1 = rateDao.getItemId(poi1);
				innerpoi2 = rateDao.getItemId(poi2);
			} catch (NullPointerException exc) {
				continue;
			}		
			ArrayList<Poi2Times> temp1 = null;
			if(fromPOI.containsKey(innerpoi1)){
				temp1 = fromPOI.get(innerpoi1);
			} else{
				temp1 = new ArrayList<Poi2Times>();				
			}
			temp1.add(p2t);
			fromPOI.put(innerpoi1, temp1);
				
			ArrayList<Poi2Times> temp2 = null;
			if(toPOI.containsKey(innerpoi2)){
				temp2 = toPOI.get(innerpoi2);
				
			} else{
				temp2 = new ArrayList<Poi2Times>();
			}
			temp2.add(p2t);
			toPOI.put(innerpoi2, temp2);
		}
	}

	/***************************************************************** Functional Methods 
	 * @throws IOException 
	 * @throws NumberFormatException *******************************************/

	private void readLoc(String contextPath) throws NumberFormatException, IOException {
		itemContexts = new HashMap<Integer, ItemContext>();
		BufferedReader brContext = FileIO.getReader(contextPath);
		
		String line = null;
		ItemContext ic = null;
		Location loc;
		
		nePoint = new Point2D.Double();
		nePoint.setLocation(Double.MIN_VALUE, Double.MIN_VALUE);
		swPoint = new Point2D.Double();
		swPoint.setLocation(Double.MAX_VALUE, Double.MAX_VALUE);

		while ((line = brContext.readLine()) != null) {
			String[] data = line.split("[ \t,]");

			String item = data[0];
			double latitude = Double.parseDouble(data[1]);
			double longitude = Double.parseDouble(data[2]);

			loc = new Location(item, latitude, longitude);

			int itemId = rateDao.getItemId(item);

			ic = new ItemContext(itemId);
			ic.setLocation(loc);

			itemContexts.put(itemId, ic);

			if ((latitude > nePoint.getX()) && (longitude > nePoint.getY())) {
				nePoint.setLocation(latitude, longitude);
			}
			if ((latitude < swPoint.getX()) && (longitude < swPoint.getY())) {
				swPoint.setLocation(latitude, longitude);
			}
			loc = null;
		}
	}

	/**
	 * @return number of days for a given time difference
	 */
	protected static int days(long diff) {
		return (int) TimeUnit.MILLISECONDS.toDays(diff);
	}

	public WorldGrid getWorldGrid() {
		return worldGrid;
	}
	public void setWorldGrid(WorldGrid worldGrid) {
		this.worldGrid = worldGrid;
	}

	@Override
	public String toString() {
		return Strings.toString(new Object[] { lambda, sigma, binThold, numFactors, regU, regI, numIters }, ",");
	}

}
