package librec.data;

import java.util.Comparator;
import java.util.Map;

public class LocationComparator implements Comparator<Location> {

	Map<Location, Integer> theBaseMap;

	public LocationComparator(Map<Location, Integer> aBaseMap) {
		theBaseMap = aBaseMap;
	}

	public int compare(Location a, Location b) {
		if (theBaseMap.get(a) >= theBaseMap.get(b)) {
			return -1;
		} else {
			return 1;
		}
	}

}