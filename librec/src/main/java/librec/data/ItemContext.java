// Copyright (C) 2014 Guibing Guo
//
// This file is part of LibRec.
//
// LibRec is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LibRec is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LibRec. If not, see <http://www.gnu.org/licenses/>.
//
package librec.data;

/**
 * Item-related Contextual Information
 * 
 * @author guoguibing
 * 
 */
public class ItemContext extends Context {

	// item name
	private String name;
	// item price
	private double price;
	// item category
	private String category;
	// item description
	private String description;
	//item location
	private Location location;
	//grid id
	private int gridId;
	// grid coordinates
	private int xGrid;	
	private int yGrid;

	// flag : is already in a grid
	private boolean isAlreadyInGrid;
		
	/**
	 * 
	 * @param user
	 *            fixed as -1
	 * @param item
	 *            item id
	 */
	private ItemContext(int user, int item) {
		super(-1, item);
		isAlreadyInGrid = false;
	}

	public ItemContext(int item) {
		this(-1, item);
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public int getXGrid() {
		return xGrid;
	}	

	public void setXGrid(int xGrid) {
		this.xGrid = xGrid;
	}
	
	public int getYGrid() {
		return yGrid;
	}	

	public void setYGrid(int yGrid) {
		this.yGrid = yGrid;
	}
	
	public int getGridId() {
		return gridId;
	}	

	public void setGridId(int gridId) {
		this.gridId = gridId;
	}
	
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public boolean isAlreadyInGrid() {
		return isAlreadyInGrid;
	}

	public void setAlreadyInGrid(boolean isAlreadyInGrid) {
		this.isAlreadyInGrid = isAlreadyInGrid;
	}
	
}
