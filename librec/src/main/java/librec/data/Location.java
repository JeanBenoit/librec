package librec.data;

public class Location {
	
	private String id;
    private double latitude;
    private double longitude;    
      
    public double distanceTo(Location aLocation) {
        return Location.distance(getLatitude(), getLongitude(), aLocation.getLatitude(), aLocation.getLongitude());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (Double.compare(location.latitude, latitude) != 0) return false;
        if (Double.compare(location.longitude, longitude) != 0) return false;
        if (!id.equals(location.id)) return false;

        return true;
    }

    public Location(String aId, double aLat, double aLong) {
        this.id = aId;
        this.latitude = aLat;
        this.longitude = aLong;      
    }

    public Location(Location aLocation) {
        id = aLocation.getId();
        latitude = aLocation.getLatitude();
        longitude = aLocation.getLongitude();
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id.hashCode();
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
    
    // Getters & Setters ---------------
    
    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
    
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public static double distance(double lat1, double lng1, double lat2, double lng2) {
	    double earthRadius = 6371000; 
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lng2-lng1);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    float dist = (float) (earthRadius * c);

	    return dist;
	}
    
}