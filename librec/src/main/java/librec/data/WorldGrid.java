package librec.data;

import happy.coding.io.Logs;

import java.awt.geom.Point2D;
import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

/**
 * Grids class dividing world into small grids
 * 
 * @author jb.griesner
 * 
 */

public class WorldGrid {

	// grids starting at SW : tab[0][0], to NE : tab[N][N]
	private Grid[][] grid; 
	
	// number of grids 
	private int height;	
	private int width;
	
	// for distance calculations
	private GeodeticCalculator gc;
			
	// number of grids
	private int gridsNum;
			
	// side size (in meters) of each small grid
	private double gridsSize;
	
	// lat/lon points
	private Point2D.Double swPoint;
	private Point2D.Double nePoint;		
	private Point2D.Double sePoint;
	private Point2D.Double nwPoint;	
	
	public WorldGrid(double size, Point2D.Double swPoint, Point2D.Double nePoint) throws NoSuchAuthorityCodeException, FactoryException {	 		
		this.swPoint = swPoint;
		this.nePoint = nePoint;		
		this.gridsSize = size;	
		// mind, this is lat/lon
	    sePoint = new Point2D.Double();
	    sePoint.setLocation(swPoint.getX(), nePoint.getY());
	    nwPoint = new Point2D.Double();
	    nwPoint.setLocation(nePoint.getX(), swPoint.getY());
		createGrid(gridsSize);		
	}
		
	/**
	 * @throws FactoryException 
	 * @throws NoSuchAuthorityCodeException *******************************************/
	
	public void createGrid(double granularity) throws NoSuchAuthorityCodeException, FactoryException {	
		//EPSG:3857
		CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:3857");				
		gc = new GeodeticCalculator(sourceCRS);	
		
	    // calculations of the width of the grid
	    gc.setStartingGeographicPoint(swPoint.getY(), swPoint.getX());
	    gc.setDestinationGeographicPoint(sePoint.getY(), sePoint.getX());
	    double d = gc.getOrthodromicDistance();
	    width = (int) Math.ceil(d / granularity) + 1;

	    // calculations of the height of the grid
	    gc.setDestinationGeographicPoint(nePoint.getY(), nePoint.getX());
	    d = gc.getOrthodromicDistance();
	    height = (int) Math.ceil(d / granularity) + 1;

	    gridsNum = height * width;
	    		
	    // grid creation
	    grid = new Grid[height][width];
	    grid[0][0] = new Grid(0, 0, 0, swPoint.getX(), swPoint.getY(), granularity);
	    double dist;
	    int gridId = 1;

	    for (int i = 0; i < height; i++) {
	        gc.setStartingGeographicPoint(grid[i][0].getLongitude(),grid[i][0].getLatitude());
	        dist = granularity;

	        for (int j = 1; j < width; j++) {

	            gc.setDirection(90 /* azimuth */, dist);

	            Point2D.Double p = (Point2D.Double) gc.getDestinationGeographicPoint();

	            grid[i][j] = new Grid(gridId, i, j, p.getY(), p.getX(), granularity);
	            dist += granularity;
	            gridId++;
	        }

	        if (i != height-1) {
	            gc.setDirection(0 /* azimuth */, granularity);
	            Point2D.Double p = (Point2D.Double) gc.getDestinationGeographicPoint();
	            grid[i+1][0] = new Grid(gridId, i+1, 0, p.getY(), p.getX(), granularity);
	        }	        
	    }		
		Logs.debug("Grids [{} X {}] created with {} square cells of {} meters", height, width, gridsNum, gridsSize);		
	}	
	
	//Getters & Setters
	public Grid[][] getGrid() {
		return this.grid;
	}
	public void setGrid(Grid[][] aGrid) {
		this.grid = aGrid;
	}
	public double getGridsSize() {
		return gridsSize;
	}
	public void setGridsSize(double gridsSize) {
		this.gridsSize = gridsSize;
	}
	public int getGridsNum() {
		return gridsNum;
	}
	public void setGridsNum(int gridsNum) {
		this.gridsNum = gridsNum;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public Point2D.Double getSwPoint() {
		return swPoint;
	}
	public void setSwPoint(Point2D.Double swPoint) {
		this.swPoint = swPoint;
	}
	public Point2D.Double getNePoint() {
		return nePoint;
	}
	public void setNePoint(Point2D.Double nePoint) {
		this.nePoint = nePoint;
	}
	public Point2D.Double getSePoint() {
		return sePoint;
	}
	public void setSePoint(Point2D.Double sePoint) {
		this.sePoint = sePoint;
	}
	public Point2D.Double getNwPoint() {
		return nwPoint;
	}
	public void setNwPoint(Point2D.Double nwPoint) {
		this.nwPoint = nwPoint;
	}
	public GeodeticCalculator getGc() {
		return gc;
	}
	public void setGc(GeodeticCalculator gc) {
		this.gc = gc;
	}

}