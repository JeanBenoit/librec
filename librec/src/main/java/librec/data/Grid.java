package librec.data;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import librec.ranking.GeoMF;

import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;


/**
 * Grid containing POIs
 * 
 * @author jb.griesner
 * 
 */

public class Grid {

	private int gridId;

	private int xGrid;
	private int yGrid;
	
	private double longitude;
	private double latitude;
	private double size;
	
	private Point2D.Double gswPoint;
	private Point2D.Double gnePoint;	

	private int nbInsidePOI = 0;
	private ArrayList<Integer> IdsPOI;

	public Grid(int gridId, int xGrid, int yGrid, double latitude, double longitude, double size) throws NoSuchAuthorityCodeException, FactoryException {
		this.gridId = gridId;
		this.xGrid = xGrid;
		this.yGrid = yGrid;
		this.longitude = longitude;
		this.latitude = latitude;
		this.size = size;
		IdsPOI = new ArrayList<Integer>();
						
		GeodeticCalculator ggc = new GeodeticCalculator(CRS.decode("EPSG:3857"));	
		
	    ggc.setStartingGeographicPoint(longitude, latitude);
	    double semiDiag = Math.sqrt(Math.pow(size, 2) / 2.0);	    
	    ggc.setDirection(45 /* azimuth */, semiDiag);
        Point2D.Double p1 = (Point2D.Double) ggc.getDestinationGeographicPoint();
		ggc.setDirection(-135 /* azimuth */, semiDiag);
        Point2D.Double p2 = (Point2D.Double) ggc.getDestinationGeographicPoint();
        
        gnePoint = new Point2D.Double();
        gnePoint.setLocation(p1.getY(), p1.getX());
        
		gswPoint = new Point2D.Double();
        gswPoint.setLocation(p2.getY(), p2.getX());
        
        findPOIs();
	}	

	public void findPOIs(){		
		for(int itemId = 0; itemId < GeoMF.itemContexts.size(); itemId++){
			ItemContext tempIC = GeoMF.itemContexts.get(itemId);
			if(!tempIC.isAlreadyInGrid()){				
				if(isInGrid(tempIC.getLocation().getLatitude(), tempIC.getLocation().getLongitude())){
					tempIC.setGridId(gridId);
					tempIC.setXGrid(xGrid);;
					tempIC.setYGrid(yGrid);
					tempIC.setAlreadyInGrid(true);
					GeoMF.itemContexts.put(itemId, tempIC);
					addPOI(itemId);
				}	
			}			
		}
	}

	public void addPOI(int itemId){
		IdsPOI.add(itemId);
		nbInsidePOI++;
	}

	public boolean isInGrid(double aLatitude, double aLongitude) {			    
		boolean isIn = false;
		if ( (aLongitude < gnePoint.getY()) && (aLongitude > gswPoint.getY()) && (aLatitude < gnePoint.getX()) && (aLatitude > gswPoint.getX()) ) {
			isIn = true;		
		}
		return isIn;
	}
	
	// Getters & Setters
	public double getXGrid() {
		return xGrid;
	}
	public void setXGrid(int xGrid) {
		this.xGrid = xGrid;
	}
	public double getYGrid() {
		return yGrid;
	}
	public void setYGrid(int yGrid) {
		this.yGrid = yGrid;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public ArrayList<Integer> getIdsPOI() {
		return IdsPOI;
	}
	public void setIdsPOI(ArrayList<Integer> idsPOI) {
		IdsPOI = idsPOI;
	}
	public int getGridId() {
		return gridId;
	}
	public void setGridId(int gridId) {
		this.gridId = gridId;
	}
	public int getNbInsidePOI() {
		return nbInsidePOI;
	}
	public void setNbInsidePOI(int nbInsidePOI) {
		this.nbInsidePOI = nbInsidePOI;
	}
	public Point2D.Double getGswPoint() {
		return gswPoint;
	}
	public void setGswPoint(Point2D.Double gswPoint) {
		this.gswPoint = gswPoint;
	}
	public Point2D.Double getGnePoint() {
		return gnePoint;
	}
	public void setGnePoint(Point2D.Double gnePoint) {
		this.gnePoint = gnePoint;
	}
}