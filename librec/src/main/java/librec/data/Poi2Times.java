package librec.data;

public class Poi2Times {
	
	public final String rawPOI1;
	public final String rawPOI2;
	public final String times;
	public final String time12;
	
	public Poi2Times(String rawPOI1, String rawPOI2, String times, String time12) {
		this.rawPOI1 = rawPOI1;
		this.rawPOI2 = rawPOI2;
		this.times = times;
		this.time12 = time12;
	}
	
	public String toString(){
		String result = times + ":" + rawPOI1 +"->"+ rawPOI2 + "(" + time12 + ")";
		return result;
	}
	
	@Override
	public boolean equals(Object other) {
		Boolean result = false;
	    if (!(other instanceof Poi2Times)) {
	        return false;
	    }

	    Poi2Times that = (Poi2Times) other;
	    
	    result = (this.rawPOI1.equals(that.rawPOI1)) && (this.rawPOI2.equals(that.rawPOI2)); 
	    return result;
	}
	
	@Override
	public int hashCode() {
	    int hashCode = 1;
	    
	    Integer temp1 = Integer.parseInt(rawPOI1) * 123;
	    Integer temp2 = (Integer.parseInt(rawPOI2) * 456 + temp1) * 111;
	    
	    hashCode = hashCode * 37 + temp2.hashCode();

	    return hashCode;
	}
}
